export const Microservices : {[key: string]: string} = {
    // give like this : 
    // "ms-name" : "http://13.238.209.142:[port-no.]/[context-root]"
    "auth"      : "http://13.238.209.142:8100/auth",
    "benchmark" : "http://13.238.209.142:8250/benchmark",
    "checklist" : "http://13.238.209.142:8200/checklist",
    "severity"  : "http://13.238.209.142:8300/severity"
}